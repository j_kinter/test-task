
// Menu interakce
var body = $('body'),
    siteNav = $('.site-nav'),
    toggleNav = $('#toggle-nav');

toggleNav.click(function() {
    $(this).toggleClass('active');
    body.toggleClass('overflow-hidden');
    siteNav.toggleClass('open');
});


// Tooltip pristupnost
$('.has-tooltip').each(function() {
    $(this)
    .focus(function() {
            $(this).next().attr('aria-hidden', 'false');
    })
    .blur(function() {
            $(this).next().attr('aria-hidden', 'true');
    });
});


// Charts
$.fn.newChart = function(chartOptions) {
    return $(this).each(function() {

        var barElement = $(this).find('.chart-items');
        var barMarkup = barElement.html();

        for (i = 0; i < chartOptions.chartData.length; i++) {
            barMarkup = $.fn.newChart.createBar(chartOptions.chartData[i][1], chartOptions.chartData[i][0]);
            barElement.append(barMarkup);
        }

        var legend = $(this).find('.legend');

        var legendMarkup =
            '<ul>' +
                '<li class="low">bez fronty</li>' +
                '<li class="medium">možnost fronty</li>' +
                '<li class="high">pravděpodobné čekání ve frontě</li>' +
            '</ul>';

        legend.append(legendMarkup);
    });
};

$.fn.newChart.createBar = function(fill, time){
    var bgColor;
    var fillDescription;

    if (fill >= 36 && fill <= 44) {
        bgColor = '#f88c6c';
        fillDescription = 'možnost fronty';
    } else if (fill >= 45) {
        bgColor = '#e64e21';
        fillDescription = 'pravděpodobné čekání ve frontě';
    } else {
        bgColor = '#ffc59b';
        fillDescription = 'bez fronty';
    }

    var barElement = 
        '<div class="bar-container">' +
            '<div class="bar">' +
                '<div class="fill" style="background-color:' + bgColor + '; height:' + fill * 1.5 + 'px">';

    barElement +=
                '</div>' +
            '</div>' +
            '<span class="time">' + time + '</span>' +
            '<span class="sr-text">' + fillDescription + '</span>' +
        '</div>';

    return barElement;
};